superbuild_add_project_python_pyproject(pythonsetuptoolsscm
  PACKAGE
    setuptools_scm
  DEPENDS
    pythonsetuptools
    pythonpackaging
    pythontomli
    pythontypingextensions
  LICENSE_FILES
    LICENSE
  SPDX_LICENSE_IDENTIFIER
    MIT
  SPDX_COPYRIGHT_TEXT
    "Copyright 2010-2015 by Ronny Pfannschmidt"
  )

superbuild_add_project_python_pyproject(pythonwheel
  PACKAGE
    wheel
  DEPENDS
    pythonflitcore
  LICENSE_FILES
    LICENSE.txt
  SPDX_LICENSE_IDENTIFIER
    MIT
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) 2012 Daniel Holth and contributors"
  PYPROJECT_TOML_NO_WHEEL
  )

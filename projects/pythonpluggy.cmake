superbuild_add_project_python_pyproject(pythonpluggy
  PACKAGE
    pluggy
  DEPENDS
    pythonsetuptools
    pythonsetuptoolsscm
  LICENSE_FILES
    LICENSE
  SPDX_LICENSE_IDENTIFIER
    MIT
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) 2015 holger krekel"
  )

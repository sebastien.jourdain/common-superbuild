superbuild_add_project_python_pyproject(pythoncppy
  PACKAGE
    cppy
  DEPENDS
    pythonsetuptools
    pythonsetuptoolsscm
  LICENSE_FILES
    LICENSE
  SPDX_LICENSE_IDENTIFIER
    BSD-3-Clause
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) 2014, Nucleic"
  )

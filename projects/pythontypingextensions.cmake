superbuild_add_project_python_pyproject(pythontypingextensions
  PACKAGE
    typing_extensions
  LICENSE_FILES
    LICENSE
  SPDX_LICENSE_IDENTIFIER
    PSF-2.0
  SPDX_COPYRIGHT_TEXT
    "Copyright Guido van Rossum, Jukka Lehtosalo, Łukasz Langa, Michael Lee"
  )

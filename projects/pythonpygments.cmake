superbuild_add_project_python_pyproject(pythonpygments
  PACKAGE
    pygments
  DEPENDS
    pythonsetuptools
  LICENSE_FILES
    LICENSE
    AUTHORS
  SPDX_LICENSE_IDENTIFIER
    BSD-2-Clause
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) 2006-2022 by the respective pythonpygments authors"
  )
